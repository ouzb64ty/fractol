# Fractol

![Démo](https://framagit.org/ouzb64ty/fractol/-/raw/main/img/demo.png?ref_type=heads "Démo de Fractol")

Simple générateur de fractales en C avec la SDL from scratch. (non fignolé) ✅

## Explications

En partant du principe qu'une fractale de Mandelbrot a une forme autosimilaire
définie par :
Z -> Z^2 + C

Soit Z le nombre complexe du plan complexe portant sa propre valeur récursive
et C le nombre complexe définissant la coordonnée du point appartenant ou non
à l'ensemble de Mandelbrot. Si Z après une itération de 1000 tend vers l'infini
alors le point C est noir sinon si C tend vers 0 alors le point est blanc.