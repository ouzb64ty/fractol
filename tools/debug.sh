#!/bin/bash

# Ce script permet de debugger le programme zraster avec Valgrind.
# Il affiche les leaks et permet de tracer les segfaults.
# Exemple d'utilisation :
# bash ./tools/debug.sh ./bin/zraster

if ! command -v valgrind &> /dev/null; then
    echo "Valgrind n'est pas installé."
    exit 1
fi

valgrind --leak-check=full --show-leak-kinds=all $1 $2
