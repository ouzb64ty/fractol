#include "mandelbrot.h"
#include <stdio.h>
#include <math.h>

void draw_mandelbrot(t_app *app, t_control *control) {

    SDL_Color c_black;
    SDL_Color c_custom;
    t_point current;
    long double tmp_x = 0;
    long double tmp_y = 0;
    long double mandel_x = 0;
    long double mandel_y = 0;
    int i = 0;

    c_black.r = 0;
    c_black.g = 0;
    c_black.b = 0;
    c_black.a = 255;
    for (long double px = 0; px < SCREEN_HEIGHT; px++) {
        for (long double py = 0; py < SCREEN_WIDTH; py++) {
            // Convert pixel coordinates to complex numbers
            long double real = ((px - SCREEN_WIDTH / 2.0) * control->zoom / SCREEN_WIDTH) + control->move_x;
            long double imag = ((py - SCREEN_HEIGHT / 2.0) * control->zoom / SCREEN_HEIGHT) + control->move_y;

            mandel_x = real;
            mandel_y = imag;

            for (i = 0; i < FRACTAL_ITER; i++) {
                tmp_x = (mandel_x * mandel_x) - (mandel_y * mandel_y) + real;
                tmp_y = (2.0 * (mandel_x * mandel_y)) + imag;

                mandel_x = tmp_x;
                mandel_y = tmp_y;

                // Check if the point escapes to infinity
                if ((mandel_x * mandel_x) + (mandel_y * mandel_y) > 4.0) {
                    break;
                }
            }

            current.x = px;
            current.y = py;
            c_custom.r = 150 + (i % 105);
            c_custom.g = 150 + (i % 105);
            c_custom.b = 150 + (i % 105);
            c_custom.a = 255;
            if (i == FRACTAL_ITER) {
                drawPixel(app, current, c_black);
            } else {
                drawPixel(app, current, c_custom);
            }
        }
    }
}
