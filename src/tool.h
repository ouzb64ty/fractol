#ifndef TOOL_H
# define TOOL_H

# include <SDL2/SDL.h>

# include "def.h"

void
drawPixel(t_app *app, t_point p, SDL_Color c);

#endif
