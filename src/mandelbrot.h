#ifndef MANDELBROT_H
# define MANDELBROT_H

# include <SDL2/SDL.h>

# include "def.h"
# include "tool.h"

void draw_mandelbrot(t_app *app, t_control *control);

#endif
