#include "tool.h"

void
drawPixel(t_app *app, t_point p, SDL_Color c) {

  SDL_SetRenderDrawColor(app->renderer, c.r, c.g, c.b, c.a);
  SDL_RenderDrawPoint(app->renderer, p.x, p.y);
}
