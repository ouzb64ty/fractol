#ifndef DEF_H
# define DEF_H

# include <SDL2/SDL.h>

typedef enum e_bool {
  TRUE,
  FALSE
} t_bool;

# define MSG_USAGE "usage: ./fractol\n"

/* Configuration */

# define APP_DELAY 150

# define SCREEN_WIDTH 600
# define SCREEN_HEIGHT 600

# define FRACTAL_ITER 1000

typedef struct s_app {
  SDL_Renderer *renderer;
  SDL_Window *window;
} t_app;

typedef struct s_point {
  long double x;
  long double y;
} t_point;

typedef struct s_control {
  long double zoom;
  long double move_x;
  long double move_y;
} t_control;

#endif
