#include "main.h"

static void
sdl_init(t_app *app, t_control *control) {

  int rendererFlags = SDL_RENDERER_ACCELERATED;

  if (SDL_Init(SDL_INIT_VIDEO) < 0)
    return ;
  control->move_x = 0;
  control->move_y = 0;
  control->zoom = 4;
  app->window = SDL_CreateWindow(
      "Fractol",
      SDL_WINDOWPOS_CENTERED,
      SDL_WINDOWPOS_CENTERED,
      SCREEN_WIDTH,
      SCREEN_HEIGHT,
      SDL_WINDOW_SHOWN);
  if (app->window == NULL) {
    return ;
  }
  app->renderer = SDL_CreateRenderer(app->window, -1, rendererFlags);
  if (app->renderer == NULL) {
    return ;
  }
}

static void
sdl_event(t_app *app, t_control *control) {

  SDL_Event event;

  (void)app;
  while (SDL_PollEvent(&event)) {
    if (event.type == SDL_QUIT) {
      exit(0);
    } else if (event.type == SDL_KEYDOWN) {
      switch (event.key.keysym.sym) {
        case SDLK_LEFT:
          control->move_x -= 0.1;
          break ;
        case SDLK_RIGHT:
          control->move_x += 0.1;
          break ;
        case SDLK_UP:
          control->move_y -= 0.1;
          break ;
        case SDLK_DOWN:
          control->move_y += 0.1;
          break ;
        case SDLK_a:
          control->zoom += 0.1;
          break ;
        case SDLK_z:
          control->zoom -= 0.1;
          break ;
      }
    }
  }
}

static void
sdl_render(t_app *app) {

  SDL_RenderPresent(app->renderer);
}

static void
sdl_prepare(t_app *app, t_control *control) {

  draw_mandelbrot(app, control);
  return ;
}

static int
sdl_core(void) {

  t_app app;
  t_control control;

  memset(&app, 0, sizeof(t_app));
  memset(&control, 0, sizeof(t_control));
  sdl_init(&app, &control);
  if (app.window == NULL || app.renderer == NULL)
    return -1;
  while (1) {
    sdl_prepare(&app, &control);
    sdl_render(&app);
    sdl_event(&app, &control);
    SDL_Delay(APP_DELAY);
  }
  return 1;
}

int
main(int argc, char *argv[]) {

  int ret = 0;

  ret = sdl_core();
  if (ret == -1)
    return 1;
  return 0;
}
